import pickle

from flask import Flask, request
from flask_json import FlaskJSON, JsonError, as_json

import pickle

app = Flask(__name__)
# Don't add an extra "status" property to JSON responses - this would break the API contract
app.config['JSON_ADD_STATUS'] = False
# Don't sort properties alphabetically in response JSON
app.config['JSON_SORT_KEYS'] = False

json_app = FlaskJSON(app)


@json_app.invalid_json_error
def invalid_request_error(e):
    """Generates a valid ELG "failure" response if the request cannot be parsed"""
    raise JsonError(status_=400, failure={'errors': [
        {'code': 'elg.request.invalid', 'text': 'Invalid request message'}
    ]})


@app.route('/process', methods=['POST'])
@as_json
def process_request():
    """Main request processing logic - accepts a JSON request and returns a JSON response."""
    data = request.get_json()
    # sanity checks on the request message
    if (data.get('type') != 'text') or ('content' not in data):
        invalid_request_error(None)

    content = data['content']
    result = do_nlp(content)
    return dict(response={'type': 'annotations', 'annotations': result})


@app.before_first_request
def load_models():
    global model
    with open('src/notebooks/cross_genre.pickle', "rb") as m:
        model = pickle.load(m)


def do_nlp(content):
    #implementation for a single sentence
    sentences = [content]
    sentence_anns = []

    # Predict values for test dataset
    predictions = model.predict(sentences)

    # jsonify the result
    for sent, label in zip(sentences, predictions):
        sentence_anns.append({'start': 0, 'end': len(sent), 'features': {'label': label}})
    return {'Sentence': sentence_anns}


if __name__ == '__main__':
    app.run()
